from django.urls import path
from .views import LoginHomeView, HomePageView, SignOutView


urlpatterns = [
    path('', LoginHomeView.as_view(), name='login_home'),
    path('inicio/', HomePageView.as_view(), name='home'),
    path('logout/', SignOutView.as_view(), name='logout'),
]