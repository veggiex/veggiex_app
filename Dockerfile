FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /core
WORKDIR /core
ADD requirements.txt /core/
RUN pip install -r requirements.txt
ADD . /core/