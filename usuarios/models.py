from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class DatosPersonales(models.Model):
    primer_nombre = models.CharField(max_length=100, verbose_name="Primer nombre")
    segundo_nombre = models.CharField(max_length=100, verbose_name="Segundo nombre", blank=True, null=True)
    apellido_paterno = models.CharField(max_length=200, verbose_name="Apellido paterno")
    apellido_materno = models.CharField(max_length=200, verbose_name="Apellido materno", blank=True, null=True)
    email = models.EmailField(max_length=200, verbose_name="Email", blank=True, null=True)
    telefono = models.CharField(max_length=10, verbose_name="Telefono")
    usuario = models.OneToOneField(User, verbose_name="Usuario", on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")

    class Meta:
        verbose_name = "datos personales"
        verbose_name_plural = "datos personales"
        ordering = ['-created']

    def __str__(self):
        return "{} {}".format(self.primer_nombre, self.apellido_paterno)
