from django.urls import path
from .views import DatosPersonelesUpdate, DatosPersonalesCreateView

urlpatterns = [
    path('create/', DatosPersonalesCreateView.as_view(), name='create_datospersonales'),
    path('update/', DatosPersonelesUpdate.as_view(), name='update_datospersonales'),
]
