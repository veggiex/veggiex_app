from django import forms
from .models import DatosPersonales


class DatosPersonalesForm(forms.ModelForm):
    class Meta:
        model = DatosPersonales
        fields = ['primer_nombre', 'segundo_nombre',
                  'apellido_paterno', 'apellido_materno', 'email', 'telefono']
