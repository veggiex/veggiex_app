from django.contrib import admin
from .models import DatosPersonales
# Register your models here.

class DatosPersonalesAdmin(admin.ModelAdmin):
    readonly_fields = ('created',)
    list_display = ('primer_nombre', 'apellido_paterno')


admin.site.register(DatosPersonales, DatosPersonalesAdmin)
