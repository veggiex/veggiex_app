from django.shortcuts import render
from django.views.generic.edit import UpdateView, CreateView
from django.urls import reverse_lazy
from .forms import DatosPersonalesForm
from .models import DatosPersonales
# Create your views here.


class DatosPersonelesUpdate(UpdateView):
    form_class = DatosPersonalesForm
    success_url = reverse_lazy('home')
    template_name = 'usuarios/datospersonales_form.html'

    def get_object(self):
        datospersonales, create = DatosPersonales.objects.get_or_create(usuario=self.request.user)
        return datospersonales

#sin formulario
class DatosPersonalesCreateView(CreateView):
     form_class = DatosPersonalesForm
     #model = DatosPersonales
     #fields = ['primer_nombre', 'segundo_nombre','apellido_paterno', 'apellido_materno', 'email', 'telefono']
     success_url = reverse_lazy('home')
     template_name = 'usuarios/datospersonales_form.html'

     def form_valid(self, form):
          form.instance.usuario = self.request.user
          return super(DatosPersonalesCreateView, self).form_valid(form)
